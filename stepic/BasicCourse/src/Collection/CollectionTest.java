package Collection;

import java.util.*;

public class CollectionTest {

    public static void main(String[] args) {
        Integer[] int1 = new Integer[]{1,2,3};
        Integer[] int2 = new Integer[]{0,1,2};
        Set<Integer> set1 = new HashSet<>(Arrays.asList(int1));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(int2));

        System.out.println(symmetricDifference(set1, set2).toString());

        removeEven();
    }

    public static <T> Set<T> symmetricDifference(Set<? extends T> set1, Set<? extends T> set2) {
        Set<T> ns = new HashSet<>();
        Set<T> _set1 = new HashSet<>(set1);
        Set<T> _set2 = new HashSet<>(set2);
        _set1.removeAll(set2);
        _set2.removeAll(set1);
        ns.addAll(_set1);
        ns.addAll(_set2);
        return ns;
    }

    public static void removeEven(){
        Scanner in = new Scanner(System.in);

        if (in.hasNextLine()) {
            String input = in.nextLine();

            String[] data = input.split(" ");

            ArrayDeque<String> result = new ArrayDeque<>();

            for (int i = 0; i < data.length; i++) {
                if ((i % 2) != 0) {
                    result.add(data[i]);
                }
            }

            Iterator<String> it = result.descendingIterator();
            while (it.hasNext()) {
                String e = it.next();
                System.out.print(e + " ");
            }
        }
    }
}
