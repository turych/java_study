package Logging;

import Logging.Mailer.MailPackage;
import Logging.Mailer.MailService;
import Logging.Mailer.Sendable;

public class Inspector implements MailService{

    private static final String WEAPONS = "weapons";
    private static final String BANNED_SUBSTANCE = "banned substance";
    private static final String STOLEN = "stones";

    @Override
    public Sendable processMail(Sendable mail) {
        if (mail instanceof MailPackage) {

            String checkText = ((MailPackage) mail).getContent().getContent();

            if (checkText.contains(WEAPONS)) {
                throw new IllegalPackageException();
            } else if (checkText.contains(BANNED_SUBSTANCE)) {
                throw new IllegalPackageException();
            } else if (checkText.contains(STOLEN)) {
                throw new StolenPackageException();
            }
        }
        return mail;
    }
}
