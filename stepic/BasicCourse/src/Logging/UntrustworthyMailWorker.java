package Logging;

import Logging.Mailer.MailService;
import Logging.Mailer.RealMailService;
import Logging.Mailer.Sendable;

public class UntrustworthyMailWorker implements MailService  {

    private MailService[] mailService;
    private RealMailService realMailService;

    public UntrustworthyMailWorker(MailService[] mailService) {
        this.mailService = mailService;
        this.realMailService = new RealMailService();
    }

    @Override
    public Sendable processMail(Sendable mail) {
        int i = 0;
        for (; i < mailService.length; i++) {
            mail = mailService[i].processMail(mail);
        }
        return getRealMailService().processMail(mail);
    }

    public RealMailService getRealMailService() {
        return realMailService;
    }
}
