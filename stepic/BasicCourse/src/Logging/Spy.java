package Logging;

import Logging.Mailer.MailMessage;
import Logging.Mailer.MailService;
import Logging.Mailer.Sendable;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Spy implements MailService{

    private Logger LOGGER;

    public Spy(Logger LOGGER) {
        this.LOGGER = LOGGER;
    }

    @Override
    public Sendable processMail(Sendable mail) {

        String spyKeyworld = "Austin Powers";

        if (mail instanceof MailMessage) {
            if (mail.getFrom().equals(spyKeyworld) || mail.getTo().equals(spyKeyworld)) {
                LOGGER.log(Level.WARNING ,"Detected target mail correspondence: from {0} to {1} \"{2}\"",
                        new Object[] {mail.getFrom(), mail.getTo(), ((MailMessage) mail).getMessage()});
            } else {
                LOGGER.log(Level.INFO, "Usual correspondence: from {0} to {1}",
                        new Object[]{mail.getFrom(), mail.getTo()});
            }
        }

        return mail;
    }
}
