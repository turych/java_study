package Logging;

import Logging.Mailer.MailMessage;
import Logging.Mailer.MailPackage;
import Logging.Mailer.MailService;
import Logging.Mailer.Package;

import java.util.logging.Logger;

public class TestLogging {
    public static void main(String[] args) {

        Logger logger = Logger.getLogger(TestLogging.class.getName());

        MailPackage pkg = new MailPackage("from", "toM", new Package("gfderffdg", 1));
        MailMessage message = new MailMessage("Austin Powerss", "Austin Poswers", "Message");

        Inspector inspector = new Inspector();
        Thief thief = new Thief(5);
        Spy spy = new Spy(logger);

        MailService[] mailService = new MailService[]{spy,thief,inspector};

        UntrustworthyMailWorker umw = new UntrustworthyMailWorker(mailService);
        umw.processMail(pkg);
    }
}
