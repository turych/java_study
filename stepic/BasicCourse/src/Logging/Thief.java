package Logging;

import Logging.Mailer.MailPackage;
import Logging.Mailer.MailService;
import Logging.Mailer.Package;
import Logging.Mailer.Sendable;

public class Thief implements MailService{

    private int minPrice;
    private int stolenValue = 0;

    public Thief(int minPrice) {
        this.minPrice = minPrice;
    }

    @Override
    public Sendable processMail(Sendable mail) {
        if (mail instanceof MailPackage) {
            Package mPackage = ((MailPackage) mail).getContent();
            if (mPackage.getPrice() >= getMinPrice()) {
                stolenValue += mPackage.getPrice();
                return new MailPackage(
                    mail.getFrom(),
                    mail.getTo(),
                    new Package("stones instead of " + ((MailPackage) mail).getContent().getContent(), 0)
                );
            }
        }
        return mail;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public int getStolenValue() {
        return stolenValue;
    }
}
