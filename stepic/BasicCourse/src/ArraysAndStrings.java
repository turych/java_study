public class ArraysAndStrings {

    /**
     * Checks if given <code>text</code> is a palindrome. (Example: Madam, I'm Adam!)
     *
     * @param text any string
     * @return <code>true</code> when <code>text</code> is a palindrome, <code>false</code> otherwise
     */
    public static boolean isPalindrome(String text) {
        String str = text.replaceAll( "[^a-zA-Z0-9]", "").toLowerCase();
        StringBuilder reversed = new StringBuilder(str);
        return str.contentEquals(reversed.reverse());
    }

    public static boolean isPalindrome2 (String text) {
        text = text.toLowerCase().replaceAll("[^a-z0-9]", "");
        int len = text.length();
        // Сравниваем попарно символы, двигаясь от краёв к середине
        // Если находим хоть одну разную пару, то возвращаем false
        // Если не нашли, то возвращаем true
        for (int i=0; i<(len/2); i++){
            if (text.charAt(i) != text.charAt(len - 1 - i)) return false;
        }
        return true;
    }
}
