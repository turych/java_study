package Robot;

public class Command {

    public static void moveRobot(Robot robot, int toX, int toY) {

        if (toX != robot.getX()) {
            int stepsX = Math.abs(toX) + Math.abs(robot.getX());

            if (toX > robot.getX()) {
                rotate(robot, Direction.RIGHT);
            } else {
                rotate(robot, Direction.LEFT);
            }
            for (int i = 0; i < stepsX; i++) {
                robot.stepForward();
            }
        }

        if (toY != robot.getY()) {
            if (toY > robot.getY()) {
                rotate(robot, Direction.UP);
            } else {
                rotate(robot, Direction.DOWN);
            }
            int stepsY = Math.abs(toY) + Math.abs(robot.getY());
            for (int i = 0; i < stepsY; i++) {
                robot.stepForward();
            }
        }
    }

    public static void rotate(Robot robot, Direction direction){
        while (robot.getDirection() != direction){
            robot.turnRight();
        }
    }

}
