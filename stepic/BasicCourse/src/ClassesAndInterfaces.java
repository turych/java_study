import java.util.function.DoubleUnaryOperator;

public class ClassesAndInterfaces {

    public static double integrate(DoubleUnaryOperator f, double a, double b) {
        double result = 0;
        double step = 0.000001;
        while (a < b) {
            result += step * f.applyAsDouble(a);
            a += step;
        }
        return result;
    }
}
