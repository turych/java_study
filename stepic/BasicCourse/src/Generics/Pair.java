package Generics;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Pair <T1, T2> {

    private T1 first;
    private T2 second;

    private Pair(T1 p1, T2 p2) {
        this.first = p1;
        this.second = p2;
    }

    public static <T1, T2> Pair<T1, T2> of (T1 first, T2 second) {
        return new Pair<>(first, second);
    }

    public T1 getFirst() {
        return first;
    }

    public T2 getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(first, pair.first) &&
                Objects.equals(second, pair.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    public void ifPresent(BiConsumer<? super T1, ? super T2> consumer) {
        if (first != null && second != null)
            consumer.accept(first, second);
    }
}
