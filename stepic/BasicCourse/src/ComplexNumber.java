/**
 * Дан класс ComplexNumber.
 * Переопределите в нем методы equals() и hashCode() так, чтобы equals()
 * сравнивал экземпляры ComplexNumber по содержимому полей re и im,
 * а hashCode() был бы согласованным с реализацией equals().
 */

public final class ComplexNumber {
    private final double re;
    private final double im;

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ComplexNumber) {
            ComplexNumber comparable = (ComplexNumber) obj;
            int res = Double.compare(re, comparable.getRe()) + Double.compare(im, comparable.getIm());
            if (res == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        long hash = Double.doubleToLongBits(getIm()) + Double.doubleToLongBits(getRe());
        return (int)(hash - (hash >>> 32));
    }
}