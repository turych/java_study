package MailService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Message<T>> {

    Map<String, List<T>> mailBox = new MailBoxMap<>();

    @Override
    public void accept(Message<T> message) {
        if (mailBox.containsKey(message.getTo())) {
            mailBox.get(message.getTo()).add(message.getContent());
        } else {
            List<T> contentList = new ArrayList<>();
            contentList.add(message.getContent());
            mailBox.put(message.getTo(), contentList);
        }
    }

    public Map<String, List<T>> getMailBox() {
        return mailBox;
    }
}
