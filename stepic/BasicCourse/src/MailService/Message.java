package MailService;

public interface Message<T> {

    String getFrom();

    String getTo();

    T getContent();
}
