package MailService;

import java.util.ArrayList;
import java.util.HashMap;

public class MailBoxMap<K, V> extends HashMap {

    @Override
    public Object get(Object key) {
        if(!super.containsKey(key)) {
            return new ArrayList<>();
        } else {
            return super.get(key);
        }
    }
}
