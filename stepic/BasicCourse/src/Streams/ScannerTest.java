package Streams;

import java.util.Scanner;

public class ScannerTest {

    public static void main(String[] args){
        double summa = 0;
        try (Scanner scan = new Scanner(System.in)){
            while(scan.hasNext()){
                if(scan.hasNextDouble()){
                    summa += Double.parseDouble(scan.next());
                } else {
                    scan.next();
                }
            }
            System.out.printf("%.6f", summa);
        } catch(Exception e){
            System.out.printf("%.6f", 0.000000);
        }
    }
}
