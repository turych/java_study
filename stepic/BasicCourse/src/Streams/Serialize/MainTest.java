package Streams.Serialize;

import java.io.*;

public class MainTest {

    public static void main(String[] args) {
        Animal[] animalT = new Animal[]{
                new Animal("Cat"),
                new Animal("Dog"),
                new Animal("Mouse")
        };
        try {
            byte[] data = serialize(2, animalT);
            Animal[] animal = deserializeAnimalArray(data);
            for (Animal a : animal) {
                System.out.println(a.toString());
            }
        } catch (IOException e) {
            e.getMessage();
        }


    }

    public static Animal[] deserializeAnimalArray(byte[] data) {

        ByteArrayInputStream byteStream = new ByteArrayInputStream(data);
        try (ObjectInputStream in = new ObjectInputStream(byteStream)) {

            int count = in.readInt();
            Animal[] animals = new Animal[count];

            for (int i = 0;i < count; i++) {
                animals[i] = (Animal) in.readObject();
            }
            return animals;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    private static byte[] serialize(int count, Object[] objects) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteStream);

        out.writeInt(count);

        for (Object object : objects) {
            out.writeObject(object);
        }

        out.flush();
        return byteStream.toByteArray();
    }

}
