package Streams;

public class StreamTest {

    public static void main(String[] args) {

        // readAsString
//        byte[] bytes = new byte[] {48, 49, 50, 51};
//
//        try (ByteArrayInputStream in = new ByteArrayInputStream(bytes)) {
//            System.out.println(Streams.readAsString(in, Charset.forName("ASCII")));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // pseudoRandomStream
//        List<Integer> actual = new ArrayList<>();
//        Streams.pseudoRandomStream(13).limit(30).forEach(actual::add);
//        System.out.println(actual.toString());


        // findMinMax

//        Stream stream = Arrays.stream(new Integer[]{10, 20, 1, 5, 8, 94, 1, -52, 0});
//        Comparator<Integer> comparator = Integer::compare;
//        final boolean[] failFlag = {true};
//        BiConsumer<Integer, Integer> biConsumer = (min, max) -> {
//            assertEquals(new Integer(-52), min);
//            assertEquals(new Integer(94), max);
//            failFlag[0] = false;
//        };
//
//        Streams.findMinMax(stream, comparator, biConsumer);


//        Streams.mostCommonWords();

    }
}
