package Streams;

public class ByteStreamTest {
    public static void main(String[] args) throws Exception {

        if (System.in.available() > 0) {

            int prev = System.in.read();
            int x = System.in.read();

            while (x != -1) {
                //System.out.println("prev " + prev);
                //System.out.println("x " + x);

                if (prev == 13 && x == 10) {
                    //System.out.println("write + 10");
                } else {
                    //System.out.println("write " + prev);
                    System.out.write((byte) prev);
                }
                prev = x;
                x = System.in.read();

                //System.out.println("------");
            }
            System.out.write((byte) prev);
            System.out.flush();
        }
    }
}
