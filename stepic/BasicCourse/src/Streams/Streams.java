package Streams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {

    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        int res = 0;
        int idata;
        while((idata = inputStream.read()) != -1) {
            res = Integer.rotateLeft(res, 1) ^ idata;
        }
        return res;
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream, charset);

        int x = reader.read();
        StringBuilder str = new StringBuilder();

        while (x != -1) {
            str.append((char) x);
            x = reader.read();
        }

        return str.toString();
    }

    public static IntStream pseudoRandomStream(int seed) {
        return IntStream.iterate(seed, n -> (n * n / 10) % 1000);
    }

    public static <T> void findMinMax(
            Stream<? extends T> stream,
            Comparator<? super T> order,
            BiConsumer<? super T, ? super T> minMaxConsumer) {

        Object[] minmax = new Object[2];

        stream.forEach(x -> {
            if (minmax[0] == null || order.compare(x, (T)minmax[0]) < 0) {
                minmax[0] = x;
            }
            if (minmax[1] == null || order.compare(x, (T)minmax[0]) > 0) {
                minmax[1] = x;
            }
        });

        minMaxConsumer.accept((T) minmax[0], (T)minmax[1]);
    }

    public static void mostCommonWords(){
        Scanner in = new Scanner(System.in);
        String str = in.nextLine().toLowerCase();
        String[] arr = str.split("[\\p{Punct}\\s]+");

        Map<String, Integer> counted = Arrays.stream(arr)
                .collect(HashMap::new, (map, word) -> {
                    if (map.containsKey(word)) {
                        map.put(word, map.get(word) + 1);
                    } else {
                        map.put(word, 1);
                    }
                }, HashMap::putAll);

        List<Map.Entry<String, Integer>> list = new ArrayList<>(counted.entrySet());
        list.sort((e1, e2) -> {
            if (e1.getValue() == e2.getValue()) {
                return e1.getKey().compareTo(e2.getKey());
            } else if (e1.getValue() > e2.getValue()) {
                return -1;
            } else {
                return 1;
            }
        });

        for (int i = 0; i < (list.size() >= 10 ? 10 : list.size()); i++) {
            System.out.println(list.get(i).getKey());
        }
    }
}
