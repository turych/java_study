import java.math.BigInteger;

public class ControlStructures {
    /**
     * Calculates factorial of given <code>value</code>.
     *
     * @param value positive number
     * @return factorial of <code>value</code>
     */
    public static BigInteger factorial(int value) {
        BigInteger factorial = new BigInteger(String.valueOf(1));
        for (int i = 1; i <= value; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }

    /**
     * Merges two given sorted arrays into one
     *
     * @param a1 first sorted array
     * @param a2 second sorted array
     * @return new array containing all elements from a1 and a2, sorted
     */
    public static int[] mergeArrays(int[] a1, int[] a2) {

        int[] merged = new int[a1.length + a2.length];

        int a1Index = 0;
        int a2Index = 0;
        int i = 0;

        while (a1Index < a1.length && a2Index < a2.length) {
            if(a1[a1Index] < a2[a2Index]) {
                merged[i++] = a1[a1Index++];
            } else {
                merged[i++] = a2[a2Index++];
            }
        }

        while (a1Index < a1.length) {
            merged[i++] = a1[a1Index++];
        }

        while (a2Index < a2.length) {
            merged[i++] = a2[a2Index++];
        }

        return merged;
    }

    public static String printTextPerRole(String[] roles, String[] textLines) {

        StringBuilder text = new StringBuilder();

        int i = 1;

        for (String role : roles) {
            text.append(role + ":\n");
            for (String line : textLines) {
                if (line.startsWith(role + ":")) {
                    text.append(line.replaceFirst(role + ":", i + ")") + "\n");
                }
                i++;
            }
            i = 1;
            text.append("\n");
        }

        return text.toString();
    }
}
