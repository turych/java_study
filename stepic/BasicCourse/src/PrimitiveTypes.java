public class PrimitiveTypes {

    public static boolean booleanExpression(boolean a, boolean b, boolean c, boolean d) {
        //return (a?1:0) + (b?1:0) + (c?1:0) + (d?1:0) == 2;
        return ((a != b) && (c != d)) || ((a != c) && (b != d));
    }

    public static int leapYearCount(int year) {
        return year / 4 - year / 100 + year / 400;
    }

    public static boolean doubleExpression(double a, double b, double c) {
        double EPSILON = 0.0001;
        return Math.abs(c - a - b) < EPSILON;
    }

    /**
     * Flips one bit of the given <code>value</code>.
     *
     * @param value     any number
     * @param bitIndex  index of the bit to flip, 1 <= bitIndex <= 32
     * @return new value with one bit flipped
     */
    public static int flipBit(int value, int bitIndex) {
        return value ^= 1 << bitIndex - 1 ;
    }
}
