package SpamFilter;

public interface TextAnalyzer {
    Label processText(String text);
}