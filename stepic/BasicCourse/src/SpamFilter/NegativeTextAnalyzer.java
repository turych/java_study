package SpamFilter;

public class NegativeTextAnalyzer extends KeywordAnalyzer implements TextAnalyzer {

    private String[] negative = new String[]{":(", "=(", ":|"};

    @Override
    protected String[] getKeywords() {
        return negative;
    }

    @Override
    protected Label getLabel() {
        return Label.NEGATIVE_TEXT;
    }
}
