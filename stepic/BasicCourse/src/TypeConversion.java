public class TypeConversion {

    public static char charExpression(int a) {
        return (char) ((int)'\\' + a);
    }

    /**
     * Checks if given <code>value</code> is a power of two.
     *
     * @param value any number
     * @return <code>true</code> when <code>value</code> is power of two, <code>false</code> otherwise
     */
    public static boolean isPowerOfTwo(int value) {
        // if negative
        int n = Math.abs(value);

        /*
        В числе 2 в степени n в единицу выставлен только (n+1)-й бит.
        Все остальные — нули.
        Например, числа 16 и 32 имеют двоичный вид, соответственно, 10000 и 100000.
        32 & 31 == 0 //true

        100000
        &
        011111
        =
        000000

        http://www.pvsm.ru/java/39711
         */
        return (n != 0) && ( n & (n - 1) ) == 0;
    }
}
