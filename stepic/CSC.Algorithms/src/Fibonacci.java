import java.util.Scanner;

public class Fibonacci {


    public static void main(String[] args) {
        lastFibbByCycle();
    }

    /**
     * Дано целое число 1≤n≤40, необходимо вычислить n-е число Фибоначчи
     * (напомним, что F0=0, F1=1 и Fn=Fn−1+Fn−2 при n≥2).
     *
     * Sample Input: 3
     * Sample Output: 2
     */
    public static void lastFibbByCycle() {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int N = scanner.nextInt();

            if(N <= 1) {
                System.out.println(1);
                System.exit(0);
            }

            int F1 = 0;
            int F2 = 1;
            for (int i = 0; i < N; i++) {
                F2 = F1 + F2;
                F1 = F2 - F1;
            }
            System.out.println(F1);
        }
    }


    /**
     * Relation to the golden ratio
     */
    public static void lastFibbByGolden() {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int N = scanner.nextInt();

            if(N <= 1) {
                System.out.println(N);
                System.exit(0);
            }

            double SQRT5 = Math.sqrt(5);
            double PHI = (SQRT5 + 1) / 2;
            double fibb = (Math.pow(PHI, N) / SQRT5 + 0.5);

            System.out.println((int) fibb);
        }
    }

    /**
     * Дано число 1≤n≤10^7, необходимо найти последнюю цифру n-го числа Фибоначчи.
     *
     * Как мы помним, числа Фибоначчи растут очень быстро,
     * поэтому при их вычислении нужно быть аккуратным с переполнением.
     * В данной задаче, впрочем, этой проблемы можно избежать,
     * поскольку нас интересует только последняя цифра числа Фибоначчи:
     * если 0≤a,b≤9 — последние цифры чисел Fi и Fi+1 соответственно,
     * то (a+b)mod10 — последняя цифра числа Fi+2.
     *
     * Sample Input: 875577
     * Sample Output: 2
     */
    public static void lastFibbDigit() {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextLong()) {
            int N = (int) (scanner.nextLong() % 60);

            if(N == 1) {
                System.out.println(1);
                System.exit(0);
            }

            double SQRT5 = Math.sqrt(5);
            double PHI = (SQRT5 + 1) / 2;
            double fibb = (Math.pow(PHI, N) / SQRT5 + 0.5);

            System.out.println((int) (fibb % 10));
        }
    }
}
