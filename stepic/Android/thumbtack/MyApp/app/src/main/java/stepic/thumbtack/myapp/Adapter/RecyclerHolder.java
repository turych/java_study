package stepic.thumbtack.myapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import stepic.thumbtack.myapp.Entity.FeedItem;
import stepic.thumbtack.myapp.MainActivity;
import stepic.thumbtack.myapp.R;

public class RecyclerHolder extends RecyclerView.ViewHolder {
    public RecyclerHolder(View itemView) {
        super(itemView);
    }

    public void bind(final FeedItem item) {
        TextView title = itemView.findViewById(R.id.item_title);
        TextView description = itemView.findViewById(R.id.item_description);
        final ImageView thumbnail = itemView.findViewById(R.id.item_thumbnail);

        title.setText(item.getTitle());
        description.setText(Html.fromHtml(item.getDescription()));
        Picasso.with(thumbnail.getContext()).load(item.getThumbnail()).into(thumbnail);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity activity = (MainActivity) thumbnail.getContext();
//                activity.showArticle(item.getLink());
                activity.playMusic(item.getGuid());
            }
        });
    }

}
