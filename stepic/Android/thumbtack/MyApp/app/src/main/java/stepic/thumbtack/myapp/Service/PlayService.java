package stepic.thumbtack.myapp.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import stepic.thumbtack.myapp.MainActivity;
import stepic.thumbtack.myapp.R;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class PlayService extends Service {

    private MediaPlayer player = null;
    private Notification.Builder nBuilder = null;
    private NotificationManager nm;
    // Notification ID
    private final int N_ID = 111;

    @Override
    public void onCreate() {
        super.onCreate();
        nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();
        if (action != null && action.equals("stop")) {
            player.stop();
            getNotificationManager().cancel(N_ID);
            stopSelf();
            return START_NOT_STICKY;
        }

        if (player != null) player.stop();

        Bundle extras = intent.getExtras();

        if (extras != null) {
            String url = extras.getString("mp3");
            player = new MediaPlayer();
            try {
                player.setDataSource(this, Uri.parse(url));
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(final MediaPlayer mediaPlayer) {
                        mediaPlayer.start();

                        final Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                if (!mediaPlayer.isPlaying()) {
                                    timer.cancel();
                                    return;
                                }
                                String pos = String.valueOf(mediaPlayer.getCurrentPosition() / 1000);
                                String dur = String.valueOf(mediaPlayer.getDuration() / 1000);
                                nBuilder.setContentText(pos + " / " + dur + " sec");
                                getNotificationManager().notify(N_ID, nBuilder.build());
                            }
                        }, 1000, 1000);
                    }
                });
                player.prepareAsync();
                showNotification();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (player != null) player.stop();
        super.onDestroy();
    }

    private void showNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        Intent intentStop = new Intent(this, PlayService.class).setAction("stop");
        PendingIntent pendingStop = PendingIntent.getService(
                this, 0, intentStop, PendingIntent.FLAG_CANCEL_CURRENT
        );

        nBuilder = new Notification.Builder(this);
        nBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("MP3")
                .setContentText("")
                .setContentIntent(PendingIntent.getActivity(
                        this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT))
                .addAction(R.mipmap.ic_launcher, "Stop play", pendingStop)
                .setAutoCancel(true)
                .setOngoing(false);

        getNotificationManager().notify(N_ID, nBuilder.build());
    }

    private NotificationManager getNotificationManager() {
        return nm;
    }
}
