package stepic.thumbtack.myapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.realm.RealmList;
import stepic.thumbtack.myapp.Entity.FeedItem;
import stepic.thumbtack.myapp.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerHolder> {

    private RealmList<FeedItem> items;

    public RecyclerAdapter(RealmList<FeedItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.feed_list_item, parent, false);

        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {
        FeedItem item = items.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
