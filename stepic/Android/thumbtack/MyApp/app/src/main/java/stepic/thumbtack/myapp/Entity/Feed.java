package stepic.thumbtack.myapp.Entity;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Feed extends RealmObject {
    private RealmList<FeedItem> items;

    public RealmList<FeedItem> getItems() {
        return items;
    }
}
