package stepic.thumbtack.myapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import io.realm.Realm;
import io.realm.RealmResults;
import stepic.thumbtack.myapp.Adapter.RecyclerAdapter;
import stepic.thumbtack.myapp.Entity.Feed;

public class MainFragment extends Fragment {

    private static final String TAG = "Feed";

    private RecyclerView vFeedList;
    private RequestQueue queue;
    private String url = "https://api.rss2json.com/v1/api.json?rss_url=http://feeds.twit.tv/brickhouse.xml";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        vFeedList = view.findViewById(R.id.feed_list);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() == null) {
            return;
        }

        queue = Volley.newRequestQueue(getActivity());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        final Feed feed = new Gson().fromJson(response, Feed.class);

                        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                                RealmResults<Feed> feedOld = realm.where(Feed.class).findAll();
                                if (feedOld.size() > 0) {
                                    for(Feed item : feedOld) {
                                        item.deleteFromRealm();
                                    }
                                }
                                realm.copyToRealm(feed);
                            }
                        });
                        showFeedListRecycler();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showFeedListRecycler();
            }
        });
        // Set tag
        stringRequest.setTag(TAG);
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void showFeedListRecycler() {

        RealmResults<Feed> feed = Realm.getDefaultInstance().where(Feed.class).findAll();

        if(getActivity() == null) {
            return;
        }

        if (feed.size() > 0) {
            vFeedList.setAdapter(new RecyclerAdapter(feed.get(0).getItems()));
            vFeedList.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public void onDestroyView() {
        queue.cancelAll(TAG);
        super.onDestroyView();
    }
}
