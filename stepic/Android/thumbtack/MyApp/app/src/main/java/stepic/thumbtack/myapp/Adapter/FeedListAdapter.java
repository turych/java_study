package stepic.thumbtack.myapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import stepic.thumbtack.myapp.Entity.FeedItemAPI;
import stepic.thumbtack.myapp.R;

import java.util.ArrayList;

public class FeedListAdapter extends BaseAdapter {

    private ArrayList<FeedItemAPI> items;

    public FeedListAdapter(ArrayList<FeedItemAPI> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public FeedItemAPI getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        if (view == null) {
            view = inflater.inflate(R.layout.feed_list_item, viewGroup, false);
        }
        TextView title = view.findViewById(R.id.item_title);
        TextView description = view.findViewById(R.id.item_description);
        ImageView thumbnail = view.findViewById(R.id.item_thumbnail);

        title.setText(getItem(i).getTitle());
        description.setText(getItem(i).getDescription());
        Picasso.with(thumbnail.getContext()).load(getItem(i).getThumbnail()).into(thumbnail);

        return view;
    }
}
