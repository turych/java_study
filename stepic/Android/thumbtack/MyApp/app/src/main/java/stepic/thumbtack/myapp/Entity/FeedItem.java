package stepic.thumbtack.myapp.Entity;

import io.realm.RealmObject;

public class FeedItem extends RealmObject {
    private String title;
    private String link;
    private String thumbnail;
    private String description;
    private String guid;

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public String getGuid() {
        return guid;
    }
}
