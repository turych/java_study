package stepic.thumbtack.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import stepic.thumbtack.myapp.Service.PlayService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place, new MainFragment())
                    .commitAllowingStateLoss();
        }
    }

    public void showArticle(String link) {
        Bundle bundle = new Bundle();
        bundle.putString("url", link);
        ArticleFragment fragment = new ArticleFragment();
        fragment.setArguments(bundle);

        View fragment_place_col2 = findViewById(R.id.fragment_place_col2);

        if (fragment_place_col2 != null) {
            fragment_place_col2.setVisibility(View.VISIBLE);
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place_col2, fragment)
                    .commitAllowingStateLoss();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place, fragment)
                    .addToBackStack("main")
                    .commitAllowingStateLoss();
        }
    }

    public void playMusic(String url) {
        Intent intent = new Intent(this, PlayService.class);
        intent.putExtra("mp3", url);
        startService(intent);
    }
}
