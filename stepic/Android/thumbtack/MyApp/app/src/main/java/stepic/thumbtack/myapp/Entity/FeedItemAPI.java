package stepic.thumbtack.myapp.Entity;

public class FeedItemAPI {
    private String title;
    private String link;
    private String thumbnail;
    private String description;

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getDescription() {
        return description;
    }
}
