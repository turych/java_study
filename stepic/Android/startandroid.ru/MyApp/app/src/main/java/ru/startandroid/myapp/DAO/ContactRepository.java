package ru.startandroid.myapp.DAO;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ContactRepository {

    private ContactDAO contactDAO;

    public ContactRepository(Application application) {
        AppDatabase db = Room.databaseBuilder(application, AppDatabase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
        contactDAO = db.contactDao();
    }

    public Long insert(Contact contact) throws ExecutionException, InterruptedException {
        return new insertAsyncTask(contactDAO).execute(contact).get();
    }

    private static class insertAsyncTask extends AsyncTask<Contact, Void, Long>{

        private ContactDAO mAsyncTaskDao;

        public insertAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Long doInBackground(Contact... contacts) {
            return mAsyncTaskDao.insert(contacts[0]);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }

    public List<Contact> getAll() throws ExecutionException, InterruptedException {
        return new getAllAsyncTask(contactDAO).execute().get();
    }

    private static class getAllAsyncTask extends AsyncTask<Void, Void, List<Contact>>{

        private ContactDAO mAsyncTaskDao;

        public getAllAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected List<Contact> doInBackground(Void... voids) {
            return mAsyncTaskDao.getAll();
        }

        @Override
        protected void onPostExecute(List<Contact> contacts) {
            super.onPostExecute(contacts);
        }
    }

    public void delete(Contact contact) throws ExecutionException, InterruptedException {
        new deleteAsyncTask(contactDAO).execute(contact);
    }

    private static class deleteAsyncTask extends AsyncTask<Contact, Void, Void>{

        private ContactDAO mAsyncTaskDao;

        public deleteAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            mAsyncTaskDao.delete(contacts);
            return null;
        }
    }

    public void deleteAll() throws ExecutionException, InterruptedException {
        new deleteAllAsyncTask(contactDAO).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void>{

        private ContactDAO mAsyncTaskDao;

        public deleteAllAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    public Contact get(Integer id) throws ExecutionException, InterruptedException {
        return new getAsyncTask(contactDAO).execute(id).get();
    }

    private static class getAsyncTask extends AsyncTask<Integer, Void, Contact>{

        private ContactDAO mAsyncTaskDao;

        public getAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Contact doInBackground(Integer... ids) {
            return mAsyncTaskDao.get(ids[0]);
        }

        @Override
        protected void onPostExecute(Contact contact) {
            super.onPostExecute(contact);
        }
    }

    public void update(Contact contact) throws ExecutionException, InterruptedException {
        new updateAsyncTask(contactDAO).execute(contact);
    }

    private static class updateAsyncTask extends AsyncTask<Contact, Void, Void>{

        private ContactDAO mAsyncTaskDao;

        public updateAsyncTask(ContactDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            mAsyncTaskDao.update(contacts);
            return null;
        }
    }
}
