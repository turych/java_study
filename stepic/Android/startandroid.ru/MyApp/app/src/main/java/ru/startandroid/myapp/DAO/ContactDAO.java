package ru.startandroid.myapp.DAO;

import android.arch.persistence.room.*;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ContactDAO {
    @Query("SELECT * FROM contact")
    List<Contact> getAll();

    @Insert
    Long insert(Contact contact);

    @Query("SELECT * FROM contact WHERE id LIKE :id LIMIT 1")
    Contact get(Integer id);

    @Query("DELETE FROM contact")
    void deleteAll();

    @Delete
    public void delete(Contact... contacts);

    @Update
    public void update(Contact... contacts);
}
