package dbService.dao;

import dbService.DBException;
import dbService.entity.UsersEntity;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

public class UsersDAO extends DAO{

    public UsersEntity getById(long id) throws DBException {
        UsersEntity users = (UsersEntity) getSession().get(UsersEntity.class, id);
        close();
        return users;
    }

    public UsersEntity get(String login) throws DBException {
        Criteria criteria = getSession().createCriteria(UsersEntity.class);
        UsersEntity users = ((UsersEntity) criteria.add(Restrictions.eq("login", login)).uniqueResult());
        close();
        return users;
    }

    public long insertUser(String login, String password) throws DBException {
        try {
            begin();
            Long id = (Long) getSession().save(new UsersEntity(login, password));
            commit();
            return id;
        } catch (HibernateException e) {
            rollback();
            throw new DBException(e);
        }
    }
}
