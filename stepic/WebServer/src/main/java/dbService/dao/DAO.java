package dbService.dao;

import dbService.DBException;
import dbService.DBService;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * Created by codecat on 31/08/17.
 */
public class DAO {

    private static final ThreadLocal session = new ThreadLocal();
    private static final DBService dbService = new DBService();

    public DAO() {}

    public static Session getSession() {
        Session session = (Session) DAO.session.get();
        if (session == null) {
            session = dbService.getSession().openSession();
            DAO.session.set(session);
        }
        return session;
    }

    protected void begin() {
        getSession().beginTransaction();
    }

    protected void commit() {
        getSession().getTransaction().commit();
    }

    protected void rollback() throws DBException {
        try {
            getSession().getTransaction().rollback();
        } catch (HibernateException e) {
            throw new DBException(e);
        }
        try {
            getSession().close();
        } catch (HibernateException e) {
            throw new DBException(e);
        }
        DAO.session.set(null);
    }

    public static void close() {
        getSession().close();
        DAO.session.set(null);
    }
}
