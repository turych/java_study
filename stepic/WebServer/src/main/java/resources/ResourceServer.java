package resources;

public class ResourceServer implements ResourceServerI {

    private TestResource resource;

    public ResourceServer() {
        resource = new TestResource();
    }

    @Override
    public void setResource(TestResource resource) {
        this.resource = resource;
    }

    @Override
    public String getName() {
        return resource.getName();
    }

    @Override
    public int getAge() {
        return resource.getAge();
    }
}
