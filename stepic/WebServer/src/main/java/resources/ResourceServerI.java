package resources;

public interface ResourceServerI {

    void setResource(TestResource resource);

    String getName();

    int getAge();
}