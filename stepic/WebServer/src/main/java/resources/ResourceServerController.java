package resources;

public class ResourceServerController implements ResourceServerControllerMBean {

    private ResourceServerI resourceServer;

    public ResourceServerController(ResourceServerI resourceServer) {
        this.resourceServer = resourceServer;
    }

    @Override
    public String getname() {
        return resourceServer.getName();
    }

    @Override
    public int getage() {
        return resourceServer.getAge();
    }
}
