package main;

import accountServer.AccountServer;
import accountServer.AccountServerController;
import accountServer.AccountServerControllerMBean;
import accountServer.AccountServerI;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import accounts.AccountService;
import accounts.UserProfile;
import chat.WebSocketChatServlet;
import resources.ResourceServer;
import resources.ResourceServerController;
import resources.ResourceServerControllerMBean;
import servlets.*;

import javax.management.*;
import java.lang.management.ManagementFactory;

/**
 * Описание курса и лицензия: https://github.com/vitaly-chibrikov/stepic_java_webserver
 */
public class Main {

    private static AccountServerI accountServer;
    private static ResourceServer resourceServer;

    public static void main(String[] args) throws Exception {

        AccountService accountService = new AccountService();
        accountService.addNewUser(new UserProfile("admin"));

        setAccountServer(new AccountServer(10));
        setResourceServer(new ResourceServer());

        initMBean();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(new SessionsServlet(accountService)), "/api/v1/sessions");
        context.addServlet(new ServletHolder(new SignUpServlet()), "/signup");
        context.addServlet(new ServletHolder(new SignInServlet()), "/signin");
        context.addServlet(new ServletHolder(new WebSocketChatPageServlet()), "/chat");
        context.addServlet(new ServletHolder(new WebSocketChatServlet()), "/websocket-chat");
        context.addServlet(new ServletHolder(new HomePageServlet(getAccountServer())), HomePageServlet.PAGE_URL);
        context.addServlet(new ServletHolder(new AdminServlet(getAccountServer())), AdminServlet.PAGE_URL);
        context.addServlet(new ServletHolder(new ResourceServlet(getResourceServer())), ResourceServlet.PAGE_URL);

        ResourceHandler resource_handler = new ResourceHandler();
        resource_handler.setResourceBase("public_html");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{resource_handler, context});

        // Create server
        Server server = new Server(8080);
        server.setHandler(handlers);

        // Start Server
        server.start();
        java.util.logging.Logger.getGlobal().info("Server started");
        server.join();
    }

    private static void initMBean() {
        AccountServerControllerMBean asc = new AccountServerController(getAccountServer());
        ResourceServerControllerMBean rsc = new ResourceServerController(getResourceServer());

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        try {
            ObjectName name = new ObjectName("Admin:type=AccountServerController.usersLimit");
            mbs.registerMBean(asc, name);
            ObjectName name1 = new ObjectName("Admin:type=ResourceServerController");
            mbs.registerMBean(rsc, name1);
        } catch (MalformedObjectNameException |
                NotCompliantMBeanException |
                MBeanRegistrationException |
                InstanceAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    public static AccountServerI getAccountServer() {
        return accountServer;
    }

    public static void setAccountServer(AccountServer accountServer) {
        Main.accountServer = accountServer;
    }

    public static ResourceServer getResourceServer() {
        return resourceServer;
    }

    public static void setResourceServer(ResourceServer resourceServer) {
        Main.resourceServer = resourceServer;
    }
}
