package servlets;

import com.google.gson.Gson;
import dbService.DBException;
import dbService.dao.UsersDAO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Sing Up Servlet
 */
public class SignUpServlet extends HttpServlet {
    private static final UsersDAO usersDAO = new UsersDAO();

    //sign up
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (login == null && password == null) {
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            usersDAO.insertUser(login, password);
        } catch (DBException e) {
            e.printStackTrace();
        }

        response.setContentType("text/html;charset=utf-8");
        //response.getWriter().println("sing up OK");
        response.setStatus(HttpServletResponse.SC_OK);
    }
}