package servlets;

import com.google.gson.Gson;
import dbService.DBException;
import dbService.dao.UsersDAO;
import dbService.entity.UsersEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Sing In Servlet
 */
public class SignInServlet extends HttpServlet {
    private static final UsersDAO usersDAO = new UsersDAO();

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (login == null || password == null) {
            response.setContentType("text/html;charset=utf-8");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        UsersEntity user = null;
        try {
            user = usersDAO.get(login);
        } catch (DBException e) {
            e.printStackTrace();
        }
        if (user == null || !user.getPassword().equals(login)) {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().println("Unauthorized");
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        // TO DO global session
        // session.addSession(request.getSession().getId(), profile);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().println("Authorized: " + user.getLogin());
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
