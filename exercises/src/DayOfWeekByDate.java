/**
 * Как найти день недели по дате? (for the Gregorian calendar)
 */
public class DayOfWeekByDate {
    public static void main(String[] args) {

        String [] days = new String[]{"Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"};

        int year = 2019;
        int month = 4;
        int day = 29;

        int a = (14 - month) / 12;
        int y = year - a;
        int m = month + 12 * a - 2;

        int dayOfWeek = (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12) % 7;

        System.out.println(days[dayOfWeek]);
    }
}