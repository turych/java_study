import java.util.Scanner;

/**
 * Определить номера подъезда и этажа по номеру квартиры девятиэтажного дома,
 * считая, что на каждом этаже ровно 4 квартиры,
 * а нумерация квартир начинается с первого подъезда?
 */

public class EntranceAndFloor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            // need to find apartment:
            int apartment = scanner.nextInt();
            // maximum floor per entrance
            int maxFloor = 9;
            // apartments per floor
            int aprtPerFloor = 4;

            // apartments per entrance
            int aprtInEntrance = maxFloor * aprtPerFloor;

            int entrance;
            int floor;
            entrance = apartment % aprtInEntrance == 0 ? apartment / aprtInEntrance : apartment / aprtInEntrance + 1;
            floor = apartment % aprtInEntrance == 0 ? maxFloor : apartment % aprtInEntrance / aprtPerFloor + 1;

            System.out.println("Entrance: " + entrance);
            System.out.println("Floor: " + floor);

        } else {
            System.out.println("Error. Enter int.");
        }
    }
}
