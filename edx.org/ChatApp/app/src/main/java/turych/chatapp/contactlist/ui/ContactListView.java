package turych.chatapp.contactlist.ui;

import turych.chatapp.login.entity.Account;

public interface ContactListView {
    void onContactAdded(Account account);
    void onContactChanged(Account account);
    void onContactRemoved(Account account);
}
