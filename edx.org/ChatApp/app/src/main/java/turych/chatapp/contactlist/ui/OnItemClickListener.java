package turych.chatapp.contactlist.ui;

import turych.chatapp.login.entity.Account;

public interface OnItemClickListener {
    void onItemClick(Account account);
    void onItemLongClick(Account account);
}
