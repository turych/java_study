package turych.chatapp.login.ui;

public interface LoginView {

    void showProgress(boolean showProgress);

    void setUsernameError(int messageResId);

    void setPasswordError(int messageResId);

    void navigateToMainScreen();

    void showError(String text);
}
