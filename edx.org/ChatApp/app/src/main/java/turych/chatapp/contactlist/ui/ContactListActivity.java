package turych.chatapp.contactlist.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import butterknife.BindView;
import turych.chatapp.App;
import turych.chatapp.R;
import turych.chatapp.contactlist.ContactListPresenter;
import turych.chatapp.contactlist.ContactListPresenterImpl;
import turych.chatapp.contactlist.adapter.ContactListAdapter;
import turych.chatapp.login.entity.Account;

import java.util.ArrayList;

public class ContactListActivity extends AppCompatActivity implements ContactListView,OnItemClickListener {

    @BindView(R.id.container_contact_list)
    RecyclerView recyclerView;

    private ContactListAdapter adapter;
    private ContactListPresenter contactListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        contactListPresenter = new ContactListPresenterImpl(this);


        App app = (App) getApplication();
        adapter = new ContactListAdapter(new ArrayList<Account>(), app.getImageLoader(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onContactAdded(Account account) {
        adapter.add(account);
    }

    @Override
    public void onContactChanged(Account account) {
        adapter.update(account);
    }

    @Override
    public void onContactRemoved(Account account) {
        adapter.remove(account);
    }

    @Override
    public void onItemClick(Account account) {

    }

    @Override
    public void onItemLongClick(Account account) {

    }
}
