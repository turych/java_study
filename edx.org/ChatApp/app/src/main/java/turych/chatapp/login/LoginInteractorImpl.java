package turych.chatapp.login;

public class LoginInteractorImpl implements LoginInteractor {

    private LoginRepository loginRepository;

    public LoginInteractorImpl() {
        this.loginRepository = new LoginRepositoryImpl();
    }

    @Override
    public void doSignUp(String username, String password) {
        loginRepository.signUp(username, password);
    }

    @Override
    public void doSignIn(String username, String password) {
        loginRepository.signIn(username, password);
    }

    @Override
    public void isAuthenticated() {
        loginRepository.isAuthenticated();
    }
}
