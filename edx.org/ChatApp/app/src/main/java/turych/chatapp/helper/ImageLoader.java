package turych.chatapp.helper;

import android.widget.ImageView;

public interface ImageLoader {
    void load(ImageView imageView, String URL);
}
