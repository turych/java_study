package turych.chatapp.login.entity;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Map;

@IgnoreExtraProperties
public class Account {
    public String email;
    public boolean online;
    public Map<String, Account> contacts;

    public Account() {
    }

    public Account(String email, boolean online, Map<String, Account> contacts) {
        this.email = email;
        this.online = online;
        this.contacts = contacts;
    }

    public String getEmail() {
        return email;
    }

    public boolean isOnline() {
        return online;
    }

    public Map<String, Account> getContacts() {
        return contacts;
    }
}
