package turych.chatapp.contactlist;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import turych.chatapp.firebase.FirebaseHelper;
import turych.chatapp.login.entity.Account;

public class ContactListRepositoryImpl implements ContactListRepository {

    private DatabaseReference accountReference;

    public ContactListRepositoryImpl() {
        accountReference = FirebaseHelper.getInstance().getAccountReference();
    }

    @Override
    public void signOff() {
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public String getCurrentEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser()
        return user.getEmail();
    }

    @Override
    public void removeContact(String email) {

    }

    @Override
    public void destroyContactListListener() {

    }

    @Override
    public void subscribeForContactListUpdates() {

    }

    @Override
    public void unSubscribeForContactListUpdates() {

    }

    @Override
    public void changeUserConnectionStatus(boolean online) {

    }
}
