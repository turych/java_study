package turych.chatapp.login;

public interface LoginInteractor {

    void doSignUp(String username, String password);
    void doSignIn(String username, String password);
    void isAuthenticated();
}
