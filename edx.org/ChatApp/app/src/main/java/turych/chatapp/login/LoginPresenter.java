package turych.chatapp.login;

public interface LoginPresenter {

    void onDestroy();
    boolean validateCredentials(String username, String password);
    void authenticateAccount(String username, String password);
    void registerNewAccount(String username, String password);
}
