package turych.chatapp.login.event;

public class LoginEvent {

    // Error types.
    public static final int SIGN_IN_SUCCESS = 10;
    public static final int SIGN_IN_ERROR = 20;
    public static final int SIGN_UP_SUCCESS = 30;
    public static final int SIGN_UP_ERROR = 40;
    public static final int RECOVER_SESSION_ERROR = 50;

    private int type;
    private String message;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
