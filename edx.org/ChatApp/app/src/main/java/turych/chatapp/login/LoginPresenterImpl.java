package turych.chatapp.login;

import android.text.TextUtils;
import android.widget.Toast;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import turych.chatapp.R;
import turych.chatapp.login.event.*;
import turych.chatapp.login.ui.LoginView;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        loginView = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean validateCredentials(String username, String password) {
        // Check for a valid email address.
        if (TextUtils.isEmpty(username)){
            loginView.setUsernameError(R.string.error_field_required);
            return false;
        } else if (!isEmailValid(username)){
            loginView.setUsernameError(R.string.error_invalid_email);
            return false;
        }

        // Check for a valid password.
        if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            loginView.setPasswordError(R.string.error_invalid_password);
            return false;
        }

        return true;
    }

    @Override
    public void authenticateAccount(String username, String password) {
        if (validateCredentials(username, password)) {
            loginView.showProgress(true);
            loginInteractor.doSignIn(username, password);
        }
    }

    @Override
    public void registerNewAccount(String username, String password) {
        if (validateCredentials(username, password)) {
            loginView.showProgress(true);
            loginInteractor.doSignUp(username, password);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignInSuccessEvent(SignInSuccessEvent event) {
       if (loginView != null) {
           loginView.showProgress(false);
           loginView.navigateToMainScreen();
       }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignUpSuccessEvent(SignUpSuccessEvent event) {
        if (loginView != null) {
            loginView.showProgress(false);
            loginView.navigateToMainScreen();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignInErrorEvent(SignInErrorEvent event) {
        if (loginView != null) {
            loginView.showProgress(false);
            loginView.showError(event.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignUpErrorEvent(SignUpErrorEvent event) {
        if (loginView != null) {
            loginView.showProgress(false);
            loginView.showError(event.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecoverSessionErrorEvent(RecoverSessionErrorEvent event) {
        if (loginView != null) {
            loginView.showProgress(false);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }
}
