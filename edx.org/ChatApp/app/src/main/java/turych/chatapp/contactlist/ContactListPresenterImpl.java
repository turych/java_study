package turych.chatapp.contactlist;

import turych.chatapp.contactlist.event.ContactListEvent;
import turych.chatapp.contactlist.ui.ContactListView;

public class ContactListPresenterImpl implements ContactListPresenter {

    ContactListView view;

    public ContactListPresenterImpl(ContactListView view) {
        this.view = view;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void signOff() {

    }

    @Override
    public String getCurrentUserEmail() {
        return null;
    }

    @Override
    public void removeContact(String email) {

    }

    @Override
    public void onEventMainThread(ContactListEvent event) {

    }
}
