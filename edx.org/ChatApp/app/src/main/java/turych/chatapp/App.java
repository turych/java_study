package turych.chatapp;

import android.app.Application;
import turych.chatapp.helper.GlideImageLoader;
import turych.chatapp.helper.ImageLoader;

public class App extends Application {

    private ImageLoader imageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        setupImageLoader();
    }

    private void setupImageLoader() {
        this.imageLoader = new GlideImageLoader(this);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}
