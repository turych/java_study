package turych.chatapp.login.event;

public interface LoginErrorEvent {
        String getMessage();
}
