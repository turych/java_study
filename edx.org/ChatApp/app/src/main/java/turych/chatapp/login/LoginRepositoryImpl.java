package turych.chatapp.login;

import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import org.greenrobot.eventbus.EventBus;
import turych.chatapp.firebase.FirebaseHelper;
import turych.chatapp.login.entity.Account;
import turych.chatapp.login.event.*;

public class LoginRepositoryImpl implements LoginRepository {

    private DatabaseReference accountReference;

    public LoginRepositoryImpl() {
        accountReference = FirebaseHelper.getInstance().getAccountReference();
    }

    @Override
    public void signUp(final String email, final String password) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        postEvent(new SignUpSuccessEvent());
                        signIn(email, password);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                postEvent(new SignUpErrorEvent(e.getMessage()));
            }
        });
    }

    @Override
    public void signIn(String email, String password) {
        try {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            DatabaseReference myAcountReference = accountReference.child(user.getUid());
                            myAcountReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    initSignIn(dataSnapshot);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    postEvent(new SignInErrorEvent(databaseError.getMessage()));
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            postEvent(new SignInErrorEvent(e.getMessage()));
                        }
                    });
        } catch (Exception e) {
            postEvent(new SignInErrorEvent(e.getMessage()));
        }
    }

    private void registerNewAccount(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            return;
        }

        String email = user.getEmail();
        if (email == null) {
            return;
        }

        Account account = new Account(email, true, null);
        accountReference.child(user.getUid()).setValue(account);
    }

    private void initSignIn(DataSnapshot dataSnapshot) {
        Account account = dataSnapshot.getValue(Account.class);

        if(account == null) {
            registerNewAccount();
        }

        //TODO: change connection status to online.
        postEvent(new SignInSuccessEvent());
    }

    @Override
    public void isAuthenticated() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            accountReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    initSignIn(dataSnapshot);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    postEvent(new SignInErrorEvent(databaseError.getMessage()));
                }
            });
        } else {
            postEvent(new RecoverSessionErrorEvent());
        }
    }

    private void postEvent(Object event) {
        EventBus.getDefault().post(event);
    }
}
