package turych.chatapp.contactlist.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import turych.chatapp.R;
import turych.chatapp.contactlist.ui.OnItemClickListener;
import turych.chatapp.helper.AvatarHelper;
import turych.chatapp.helper.ImageLoader;
import turych.chatapp.login.entity.Account;
import butterknife.BindView;

import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private List<Account> contactList;
    private OnItemClickListener listener;
    private ImageLoader imageLoader;

    public ContactListAdapter(List<Account> contactList, ImageLoader imageLoader, OnItemClickListener listener) {
        this.contactList = contactList;
        this.imageLoader = imageLoader;
        this.listener = listener;
    }

    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Account account = contactList.get(position);
        holder.setOnClickListener(account, listener);

        String email = account.getEmail();
        Boolean online = account.isOnline();
        String status = online ? "online" : "offline";
        int color = online ? Color.GREEN : Color.RED;

        holder.tvName.setText(email);
        holder.tvStatus.setText(status);
        holder.tvStatus.setTextColor(color);

        imageLoader.load(holder.imgAvatar, AvatarHelper.getAvatarUrl(email));
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void add(Account account){
        contactList.add(account);
    }

    public void update(Account account){
        int position = contactList.indexOf(account);
        contactList.set(position, account);
    }

    public void remove(Account account){
        contactList.remove(account);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgAvater)
        CircleImageView imgAvatar;

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvStatus)
        TextView tvStatus;

        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, view);
        }

        public void setOnClickListener(final Account account, final OnItemClickListener listener){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(account);
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onItemLongClick(account);
                    return false;
                }
            });
        }
    }
}
