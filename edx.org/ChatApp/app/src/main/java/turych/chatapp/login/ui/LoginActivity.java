package turych.chatapp.login.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import turych.chatapp.R;
import turych.chatapp.contactlist.ui.ContactListActivity;
import turych.chatapp.login.event.CanceledEvent;
import turych.chatapp.login.event.PasswordErrorEvent;
import turych.chatapp.login.event.SuccessEvent;
import turych.chatapp.login.LoginPresenter;
import turych.chatapp.login.LoginPresenterImpl;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private LoginPresenter loginPresenter;

    // UI References.
    @BindView(R.id.email) AutoCompleteTextView emailView;
    @BindView(R.id.password) EditText passwordView;
    @BindView(R.id.login_progress) View mProgressView;
    @BindView(R.id.login_error) TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenterImpl(this);
    }

    /**
     * Attempt to sing in the account.
     */
    @OnClick(R.id.btn_sign_in)
    void attemptLogin() {
        // Clear errors.
        emailView.setError(null);
        passwordView.setError(null);

        String username = emailView.getText().toString();
        String password = passwordView.getText().toString();

        loginPresenter.authenticateAccount(username, password);
    }

    @OnClick(R.id.btn_sign_up)
    void registerNewAccount() {
        // Clear errors.
        emailView.setError(null);
        passwordView.setError(null);

        String username = emailView.getText().toString();
        String password = passwordView.getText().toString();

        loginPresenter.registerNewAccount(username, password);
    }


    @Override
    public void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
    }

    @Override
    public void setUsernameError(int messageResId) {
        emailView.setError(getString(messageResId));
        emailView.requestFocus();
    }

    @Override
    public void setPasswordError(int messageResId) {
        passwordView.setError(getString(messageResId));
        passwordView.requestFocus();
    }

    @Override
    public void showError(String text) {
        tvError.setText(text);
        tvError.setVisibility(View.VISIBLE);
    }

    @Override
    public void navigateToMainScreen(){
        Intent intent = new Intent(this, ContactListActivity.class);
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessEvent(SuccessEvent successEvent) {
        showProgress(false);
        Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPasswordErrorEvent(PasswordErrorEvent passwordErrorEvent) {
        showProgress(false);
        setPasswordError(passwordErrorEvent.getMessageResId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCanceledEvent(CanceledEvent canceledEvent) {
        showProgress(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDestroy();
    }
}
