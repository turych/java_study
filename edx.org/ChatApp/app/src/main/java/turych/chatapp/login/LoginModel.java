package turych.chatapp.login;

public interface LoginModel {

    void login(String username, String password);
}
