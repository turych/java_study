package turych.chatapp.login.event;

public class SignInErrorEvent implements LoginErrorEvent{

    private String message;

    public SignInErrorEvent(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
