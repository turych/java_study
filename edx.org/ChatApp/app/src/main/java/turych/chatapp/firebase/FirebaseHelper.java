package turych.chatapp.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseHelper {

    private DatabaseReference dataReference;

    // Document`s paths
    private final static String ACCOUNT_PATH = "account";

    private static class SingletonHolder {
        private static FirebaseHelper INSTANCE = new FirebaseHelper();
    }

    public static FirebaseHelper getInstance(){
        return SingletonHolder.INSTANCE;
    }

    private FirebaseHelper(){
        dataReference = FirebaseDatabase.getInstance().getReference();
    }

    public DatabaseReference getDataReference() {
        return dataReference;
    }

    public DatabaseReference getAccountReference() {
        return getDataReference().child(ACCOUNT_PATH);
    }
}
