package turych.chatapp.login.event;

public class SignUpErrorEvent implements LoginErrorEvent{

    private String message;

    public SignUpErrorEvent(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
